import React, {useMemo, useState} from 'react';
import {StatusBar, StyleSheet} from 'react-native';
import colorsTheme from './src/utils/colorsTheme';
import AppNavigator from './src/navigation/root-switch';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Provider} from 'mobx-react';
import AuthContext from './src/context/authContext';

const App = () => {
  const [authData, setAuthData] = useState({});
  const AuthContextValue = useMemo(() => ({authData, setAuthData}), [authData]);
  return (
    <AuthContext.Provider value={AuthContextValue}>
      <SafeAreaProvider style={styles.container}>
        <StatusBar
          animated
          barStyle="dark-content"
          backgroundColor={colorsTheme.colors.white}
        />
        <AppNavigator />
      </SafeAreaProvider>
    </AuthContext.Provider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
