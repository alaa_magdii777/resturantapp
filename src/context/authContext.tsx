import React, {useState} from 'react';

const authContext = React.createContext({
  authData: {},
  setAuthData: () => {},
});
export default authContext; 
