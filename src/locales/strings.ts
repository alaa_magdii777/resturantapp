import LocalizedStrings from 'react-localization';
const Strings = new LocalizedStrings({
  en: {
    Ownername: 'Alaa Restaurant',
    foodOptions: 'Food Options',
    discountPercent: '20%',
    Discount: 'Discount',
    home: 'Home',
    Meat: 'Meat',
    Fish: 'Fish',
    MeatAndFood: 'Meat & Seafood',
    login: 'Login',
    Successfully:"Successfully",
    invalid:"email format invalid",
    passwordError:'password should be at least 6 characters',
    signUp:"Sign Up",
    DontHaveAcc:"Don't Have Account?",
    EnterName:'Enter Name',
    EnterEmail:'Enter Email',
    EnterPassword:'Enter Password',
    EnterPhone:"Enter Phone",
    errPassOrEmail:"The email has already been taken",
    CheckValidation:"Check Validation",
    MobileErr:"The mobile has already been taken",
    nameErr:"Error in Name",
    ErrEmailorPhone:"the email or phone has already been taken",
    Logout:"Logout"
  },
});

export default Strings;
