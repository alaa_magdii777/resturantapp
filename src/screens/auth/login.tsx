import React, { useState, useContext } from 'react';
import {
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import { Keyboard } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { moderateScale, scale, verticalScale } from '../../utils/scaling';
import colorsTheme from '../../utils/colorsTheme';
import Text from '../../components/text/text';
import AuthContext from '../../context/authContext';
import { authStack, rootSwitch } from '../../navigation/config/navigator';
import Strings from '../../locales/strings';
import { userLoginApiService } from '../../services/api/userLogin.service';
import { RNToasty } from 'react-native-toasty';
import { useNavigation } from '@react-navigation/native';

const LoginScreen = () => {
  const navigation = useNavigation();
  const [secure, setSecure] = useState(true);
  const { authData, setAuthData } = useContext(AuthContext);

  const [email, setEmail] = useState(null || '');
  const [isValidEmail, setEmailValidation] = useState(true);

  const [password, setPassword] = useState(null || '');
  const [isValidPassword, setPasswordValidation] = useState(true);
  const [isLoading, setisLoading] = useState(false);
  let userLogin = new userLoginApiService()

  const Login = () => {
    const values = {
      email: email,
      password: password,
    };

    if (email === null || email === '') {
      setEmailValidation(false);
    } else if (
      !/^(([^<>()[\]{}'^?\\.,!|//#%*-+=&;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i.test(
        email,
      )
    ) {
      setEmailValidation(false);
      RNToasty.Error({ title: Strings.invalid })
    } else if (password === null || password === '') {
      setPasswordValidation(false);
    } else if (password.length < 6) {
      setPasswordValidation(false);
      RNToasty.Error({ title: Strings.passwordError })
    } else if (!isValidEmail) {
      setEmailValidation(false);
    } else if (!isValidPassword) {
      setPasswordValidation(false);
    }
    userLogin.loginUser(values).then(response => {
      console.log(response, "res")
      //@ts-ignore
      setAuthData(response)
      setisLoading(true)
      {
        //@ts-ignore
        response.message === "The given data was invalid." || response.errors || response.message === "wrong email or password" ?
          RNToasty.Error({ title: Strings.CheckValidation })
          :
          RNToasty.Success({ title: Strings.Successfully })
      }
      {
        //@ts-ignore
        response.user ? navigation.navigate(rootSwitch.home)
          &&
          AsyncStorage.setItem(
            '@CurrentUser',
            //@ts-ignore
            JSON.stringify(response?.user.api_token)
          )
          :
          null
      }

    }).catch(error => {
      RNToasty.Error({ title: Strings.errPassOrEmail })
      RNToasty.Error({ title: error.message })
      setisLoading(true)
    })
  }

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.form}>
        <View style={styles.logo}>
          <Image
            source={require('../../assets/icons/logo.png')}
            style={styles.ImgLogo}
          />
        </View>
        <View style={styles.form_up_inputs}>
          <View
            style={[
              styles.up_input,
              {
                borderColor: isValidEmail
                  ? colorsTheme.colors.gray
                  : colorsTheme.colors.red,
              },
            ]}>
            <TextInput
              style={styles.input}
              value={email}
              keyboardType="email-address"
              autoCapitalize="none"
              autoCorrect={false}
              placeholder={'Enter Email'}
              onChangeText={value => {
                setEmail(value);
                setEmailValidation(true);
              }}
              onSubmitEditing={() => {
                Keyboard.dismiss();
              }}
            />
          </View>

          <View
            style={[
              styles.up_input,
              { justifyContent: 'space-between' },
              {
                borderColor: isValidPassword
                  ? colorsTheme.colors.gray
                  : colorsTheme.colors.red,
              },
            ]}>
            <TextInput
              style={styles.input}
              value={password}
              autoCorrect={false}
              autoCapitalize="none"
              secureTextEntry={secure}
              placeholder={'Enter Password'}
              onChangeText={value => {
                setPassword(value);
                setPasswordValidation(true);
              }}
              onSubmitEditing={() => {
                Keyboard.dismiss();
              }}
            />

            <TouchableOpacity
              style={{ ...styles.hide_pass_icon }}
              activeOpacity={1}
              onPress={() => setSecure(!secure)}>
              {!secure && (
                <Image
                  source={require('../../assets/icons/eye.png')}
                  style={{ height: 11, width: 16 }}
                />
              )}
              {secure && (
                <Image
                  source={require('../../assets/icons/eye_off.png')}
                  style={{ height: verticalScale(14), width: scale(14) }}
                />
              )}
            </TouchableOpacity>
          </View>

          <View style={styles.up_buttons}>
            <TouchableOpacity
              activeOpacity={0.9}
              style={styles.button}
              onPress={() => {
                Login();
              }}>
              {isLoading ?
                <ActivityIndicator color={colorsTheme.colors.white} size={'small'} /> :
                <Text title={Strings.login} style={styles.buttonText} />
              }
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity style={styles.row} onPress={() => navigation.navigate(authStack.signUp)}>
          <Text title={Strings.DontHaveAcc} style={[styles.title, { color: colorsTheme.colors.primary, marginRight: moderateScale(5) }]} />
          <Text title={Strings.signUp} style={styles.title} />
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  logo: {
    backgroundColor: colorsTheme.colors.primary,
    height: verticalScale(55),
    borderRadius: 50,
    alignSelf: 'center',
    alignItems: 'center',
    padding: moderateScale(5),
    marginTop: moderateScale(20),
  },
  ImgLogo: {
    height: verticalScale(40),
    width: scale(40),
    resizeMode: 'contain',
    alignSelf: 'center',
    alignItems: 'center',
  },
  formContainer: {},
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  form: {
    backgroundColor: colorsTheme.colors.white,
    borderRadius: 15,
    borderWidth: 5,
    borderColor: colorsTheme.colors.lightGray,
    height: verticalScale(400),
    paddingHorizontal: 0,
    margin: 20,
    marginTop: 40,
  },

  form_up_inputs: {
    paddingHorizontal: 20,
  },
  up_input: {
    height: verticalScale(55),
    overflow: 'hidden',
    borderRadius: 5,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: colorsTheme.colors.gray,
    marginTop: 20,
    alignItems: 'center',
  },
  input: {
    height: verticalScale(60),
  },
  hide_pass_icon: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  up_buttons: {
    marginTop: 20,
    flexDirection: 'row',
  },

  button: {
    height: verticalScale(55),
    borderRadius: 6,
    borderColor: colorsTheme.colors.gray,
    borderWidth: 1,
    backgroundColor: colorsTheme.colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
    width: scale(290),
    marginTop: moderateScale(10),
  },
  buttonText: {
    color: colorsTheme.colors.white,
    fontSize: moderateScale(16),
  },
  title: {
    fontSize: moderateScale(16),
    alignItems: "center",
    alignSelf: "center",
    marginTop: moderateScale(10)
  },
  row: { flexDirection: "row", alignSelf: "center" }
});

export default LoginScreen;
