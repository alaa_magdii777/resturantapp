import React, { useState, useContext } from 'react';
import {
    View,
    StyleSheet,
    Image,
    TextInput,
    TouchableOpacity,
    ScrollView,
    ActivityIndicator,
} from 'react-native';
import { Keyboard } from 'react-native';
import { moderateScale, scale, verticalScale } from '../../utils/scaling';
import colorsTheme from '../../utils/colorsTheme';
import Text from '../../components/text/text';
import { authStack, rootSwitch } from '../../navigation/config/navigator';
import Strings from '../../locales/strings';
import { RNToasty } from 'react-native-toasty';
import { useNavigation } from '@react-navigation/native';
import { userRegisterApiService } from '../../services/api/userRegister.service';
import AsyncStorage from '@react-native-community/async-storage';

const SignUpScreen = () => {
    const navigation = useNavigation();
    const [secure, setSecure] = useState(true);
    const [email, setEmail] = useState(null || '');
    const [isValidEmail, setEmailValidation] = useState(true);

    const [password, setPassword] = useState(null || '');
    const [name, setName] = useState(null || '');
    const [mobile, setMobile] = useState(null || '');
    const [isValidPassword, setPasswordValidation] = useState(true);
    const [isValidMobile, setMobileValidation] = useState(true);
    const [isValidName, setNameValidation] = useState(true);
    const [isLoading, setisLoading] = useState(false);

    let userRegister = new userRegisterApiService()

    const Register = () => {
        const values = {
            name: name,
            email: email,
            password: password,
            mobile: mobile
        };

        if (email === null || email === '') {
            setEmailValidation(false);
        } else if (
            !/^(([^<>()[\]{}'^?\\.,!|//#%*-+=&;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i.test(
                email,
            )
        ) {
            setEmailValidation(false);
            RNToasty.Error({ title: Strings.invalid })
        }
        else if (!isValidEmail) {
            setEmailValidation(false);
        }
        else if (password.length < 6) {
            setPasswordValidation(false);
            RNToasty.Error({ title: Strings.passwordError })
        }
        else if (password === null || password === '') {
            setPasswordValidation(false);
        }
        else if (!isValidPassword) {
            setPasswordValidation(false);
        } else if (!isValidMobile) {
            setMobileValidation(false);
            RNToasty.Error({ title: Strings.MobileErr })
        } else if (!isValidName) {
            setNameValidation(false);
            RNToasty.Error({ title: Strings.nameErr })
        }
        userRegister.registerUser(values).then(response => {
            setisLoading(true)
            {
                //@ts-ignore
                response.message === "The given data was invalid." || response.errors || response.message === "wrong email or password" ?
                    RNToasty.Error({ title: Strings.ErrEmailorPhone })
                    :
                    RNToasty.Success({ title: Strings.Successfully })
            }
            {
                //@ts-ignore
                response.user ? navigation.navigate(authStack.login) &&
                    //@ts-ignore
                    AsyncStorage.setItem(
                        '@CurrentUser',
                        JSON.stringify(response)
                    )
                    :
                    RNToasty.Error({ title: Strings.CheckValidation })
            }
        }).catch(error => {
            RNToasty.Error({ title: error.message })
            setisLoading(true)
        })
    }

    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.form}>
                <View style={styles.logo}>
                    <Image
                        source={require('../../assets/icons/logo.png')}
                        style={{
                            height: verticalScale(40),
                            width: scale(40),
                            resizeMode: 'contain',
                            alignSelf: 'center',
                            alignItems: 'center',
                        }}
                    />
                </View>
                <View style={styles.form_up_inputs}>
                    <View
                        style={[
                            styles.up_input,
                            {
                                borderColor: isValidEmail
                                    ? colorsTheme.colors.gray
                                    : colorsTheme.colors.red,
                            },
                        ]}>
                        <TextInput
                            style={styles.input}
                            value={name}
                            autoCapitalize="none"
                            autoCorrect={false}
                            placeholder={Strings.EnterName}
                            onChangeText={value => {
                                setName(value);
                                setNameValidation(true);
                            }}
                            onSubmitEditing={() => {
                                Keyboard.dismiss();
                            }}
                        />
                    </View>
                    <View
                        style={[
                            styles.up_input,
                            {
                                borderColor: isValidEmail
                                    ? colorsTheme.colors.gray
                                    : colorsTheme.colors.red,
                            },
                        ]}>
                        <TextInput
                            style={styles.input}
                            value={email}
                            keyboardType="email-address"
                            autoCapitalize="none"
                            autoCorrect={false}
                            placeholder={Strings.EnterEmail}
                            onChangeText={value => {
                                setEmail(value);
                                setEmailValidation(true);
                            }}
                            onSubmitEditing={() => {
                                Keyboard.dismiss();
                            }}
                        />
                    </View>

                    <View
                        style={[
                            styles.up_input,
                            { justifyContent: 'space-between' },
                            {
                                borderColor: isValidPassword
                                    ? colorsTheme.colors.gray
                                    : colorsTheme.colors.red,
                            },
                        ]}>
                        <TextInput
                            style={styles.input}
                            value={password}
                            autoCorrect={false}
                            autoCapitalize="none"
                            secureTextEntry={secure}
                            placeholder={Strings.EnterPassword}
                            onChangeText={value => {
                                setPassword(value);
                                setPasswordValidation(true);
                            }}
                            onSubmitEditing={() => {
                                Keyboard.dismiss();
                            }}
                        />

                        <TouchableOpacity
                            style={{ ...styles.hide_pass_icon }}
                            activeOpacity={1}
                            onPress={() => setSecure(!secure)}>
                            {!secure && (
                                <Image
                                    source={require('../../assets/icons/eye.png')}
                                    style={{ height: 11, width: 16 }}
                                />
                            )}
                            {secure && (
                                <Image
                                    source={require('../../assets/icons/eye_off.png')}
                                    style={{ height: verticalScale(14), width: scale(14) }}
                                />
                            )}
                        </TouchableOpacity>
                    </View>


                    <View
                        style={[
                            styles.up_input,
                            {
                                borderColor: isValidEmail
                                    ? colorsTheme.colors.gray
                                    : colorsTheme.colors.red,
                            },
                        ]}>
                        <TextInput
                            style={styles.input}
                            value={mobile}
                            autoCapitalize="none"
                            autoCorrect={false}
                            placeholder={Strings.EnterPhone}
                            onChangeText={value => {
                                setMobile(value);
                                setMobileValidation(true);
                            }}
                            onSubmitEditing={() => {
                                Keyboard.dismiss();
                            }}
                        />
                    </View>
                    <View style={styles.up_buttons}>
                        <TouchableOpacity
                            activeOpacity={0.9}
                            style={styles.button}
                            onPress={() => {
                                Register();
                            }}>
                            {isLoading ?
                                <ActivityIndicator color={colorsTheme.colors.white} size={'small'} /> :
                                <Text title={Strings.signUp} style={styles.buttonText} />
                            }
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    logo: {
        backgroundColor: colorsTheme.colors.primary,
        height: verticalScale(55),
        borderRadius: 50,
        alignSelf: 'center',
        alignItems: 'center',
        padding: moderateScale(5),
        marginTop: moderateScale(20),
    },
    formContainer: {},
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
    },
    form: {
        backgroundColor: colorsTheme.colors.white,
        borderRadius: 15,
        borderWidth: 5,
        borderColor: colorsTheme.colors.lightGray,
        height: verticalScale(550),
        paddingHorizontal: 0,
        margin: 20,
        marginTop: 40,
    },

    form_up_inputs: {
        paddingHorizontal: 20,
    },
    up_input: {
        height: verticalScale(55),
        overflow: 'hidden',
        borderRadius: 5,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: colorsTheme.colors.gray,
        marginTop: 20,
        alignItems: 'center',
    },
    input: {
        height: verticalScale(60),
    },
    hide_pass_icon: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
    },
    up_buttons: {
        marginTop: 20,
        flexDirection: 'row',
    },

    button: {
        height: verticalScale(55),
        borderRadius: 6,
        borderColor: colorsTheme.colors.gray,
        borderWidth: 1,
        backgroundColor: colorsTheme.colors.primary,
        alignItems: 'center',
        justifyContent: 'center',
        width: scale(290),
        marginTop: moderateScale(10),
    },
    buttonText: {
        color: colorsTheme.colors.white,
        fontSize: moderateScale(16),
    },
    title: {
        fontSize: moderateScale(16),
        alignItems: "center",
        alignSelf: "center",
        marginTop: moderateScale(10)
    },
    row: { flexDirection: "row", alignSelf: "center" }
});

export default SignUpScreen;
