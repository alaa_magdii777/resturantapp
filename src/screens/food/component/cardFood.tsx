import React from 'react';
import {View} from 'react-native';
import Text from '../../../components/text/text';
import {moderateScale} from '../../../utils/scaling';

export default function CardFood({navigation}: any) {
  return (
    <View style={{flex: 1, alignItems: 'center'}}>
      <Text title={'Meat Menu'} style={{marginTop: moderateScale(50)}} />
    </View>
  );
}
