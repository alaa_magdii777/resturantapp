import React, {useState} from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Image,
  StatusBar,
} from 'react-native';
import GradientView from '../../components/gradientView';
import Header from '../../components/header/header';
import Text from '../../components/text/text';
import Strings from '../../locales/strings';
import colorsTheme from '../../utils/colorsTheme';
import {moderateScale, scale, verticalScale} from '../../utils/scaling';
import CardFood from './component/cardFood';

export default function foodOptionScreen({navigation}: any) {
  const [isDetails, setIsDetails] = useState('false');
  const [isTransactions, setIsTransactions] = useState('true');
  const renderTabButtons = () => {
    return (
      <View style={styles.logoBtnsContainer}>
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={() => {
            setIsDetails('false');
            setIsTransactions('true');
          }}
          style={[
            styles.ContainerBtn,
            {
              borderBottomColor:
                isTransactions === 'true'
                  ? colorsTheme.colors.primary
                  : 'transparent',
            },
          ]}>
          <Text style={[styles.textBtn]} title={Strings.Meat} />
        </TouchableOpacity>

        <View
          style={{
            height: verticalScale(40),
            alignSelf: 'center',
            backgroundColor: colorsTheme.colors.primary,
          }}
        />

        <TouchableOpacity
          activeOpacity={0.9}
          onPress={() => {
            setIsDetails('true');
            setIsTransactions('false');
          }}
          style={[
            styles.ContainerBtn,
            {borderBottomColor: isDetails === 'true' ? 'red' : 'transparent'},
          ]}>
          <Text style={[styles.textBtn]} title={Strings.Fish} />
        </TouchableOpacity>
      </View>
    );
  };

  const renderTab = () => {
    return <View style={styles.logoContainer}>{renderTabButtons()}</View>;
  };
  return (
    <>
      <View>
        <GradientView
          Style={{
            height: verticalScale(300),
            width: '100%',
            // position: 'absolute',
            // zIndex: 1,
          }}>
          <StatusBar hidden />
          <Header
            headerScreen
            title={Strings.MeatAndFood}
            navigation={navigation}
            Style={{
              backgroundColor: colorsTheme.colors.transparent,
              paddingVertical: moderateScale(20),
            }}
          />
        </GradientView>
        <Image
          style={styles.imageBg}
          source={require('../../assets/images/headerFood.png')}
        />
      </View>
      <ScrollView style={styles.scroll}>
        <View
          style={{
            alignSelf: 'center',
            alignItems: 'center',
          }}>
          {renderTab()}
        </View>
        {isTransactions === 'true' ? (
          <CardFood navigation={navigation} />
        ) : (
          <></>
        )}
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  scroll: {
    flex: 1,
    backgroundColor: colorsTheme.colors.white,
    alignSelf: 'center',
  },
  logoBtnsContainer: {
    height: verticalScale(50),
    paddingTop: verticalScale(0),
    backgroundColor: 'white',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
  },
  logoBtn: {
    height: verticalScale(50),
    alignSelf: 'center',
    alignItems: 'center',
    elevation: 0.5,
    borderBottomWidth: 2,
  },
  logoContainer: {
    alignSelf: 'center',
    alignItems: 'center',
    alignContent: 'center',
    flexDirection: 'row',
    paddingHorizontal: moderateScale(30),
  },
  textBtn: {
    fontSize: moderateScale(24),
    width: scale(100),
    alignSelf: 'center',
    alignItems: 'center',
    alignContent: 'center',
    marginLeft: moderateScale(30),
    color: colorsTheme.colors.black,
  },
  ContainerBtn: {
    height: verticalScale(48),
    borderBottomWidth: 2,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  imageBg: {
    height: verticalScale(300),
    resizeMode: 'cover',
    justifyContent: 'center',
    alignSelf: 'center',
    width: '100%',
    borderRadius: moderateScale(5),
  },
});
