import {observer} from 'mobx-react';
import React from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  StatusBar,
  Image,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import GradientView from '../../components/gradientView';
import Header from '../../components/header/header';
import Text from '../../components/text/text';
import colorsTheme from '../../utils/colorsTheme';
import {moderateScale, scale, verticalScale} from '../../utils/scaling';

function FoodDetails({navigation, route}: any) {
  const {products, cardImg, cardName} = route.params;
  // console.log(products, 'products=====================');
  // console.log(cardName, ' cardName=====================');
  return (
    <View>
      <View>
        <GradientView Style={styles.gradientViewHeader}>
          <StatusBar hidden />
          <Header
            headerScreen
            title={cardName}
            navigation={navigation}
            Style={{
              backgroundColor: colorsTheme.colors.transparent,
              paddingVertical: moderateScale(20),
            }}
          />
        </GradientView>
        <Image style={styles.imageBg} source={{uri: cardImg}} />
      </View>
      <FlatList
        style={styles.flatList}
        data={products}
        numColumns={2}
        keyExtractor={(item,index) => index.toString()}
        renderItem={({item, index}: any) => (
          <>
            {item.length != 0 ? (
              <TouchableOpacity
                key={item.toString()}
                style={styles.cardContainer}>
                <GradientView Style={styles.gradientView}>
                  <Text title={item?.name} style={styles.txtNameProduct} />
                </GradientView>
                <Image source={{uri: item.product_img}} style={styles.Img} />
              </TouchableOpacity>
            ) : (
              <ActivityIndicator
                style={{marginTop: moderateScale(200)}}
                size="small"
                color={colorsTheme.colors.primary}
              />
            )}
          </>
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  imageBg: {
    height: verticalScale(300),
    resizeMode: 'cover',
    justifyContent: 'center',
    alignSelf: 'center',
    width: '100%',
    borderRadius: moderateScale(5),
  },
  flatList: {
    marginHorizontal: moderateScale(25),
    marginTop: moderateScale(10),
    alignSelf: 'center',
  },
  cardContainer: {
    margin: moderateScale(3),
  },
  Img: {
    height: verticalScale(159),
    width: scale(160),
  },
  gradientView: {height: verticalScale(159), width: scale(160)},
  gradientViewHeader: {
    height: verticalScale(300),
    width: '100%',
    position: 'absolute',
    zIndex: 1,
  },
  txtNameProduct: {
    color: colorsTheme.colors.white,
    fontSize: moderateScale(20),
    alignItems: 'center',
    zIndex: 1,
    top: 110,
    paddingHorizontal: moderateScale(10),
  },
});

export default FoodDetails;
