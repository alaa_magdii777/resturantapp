import React, { useContext, useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Image,
  ActivityIndicator,
} from 'react-native';
import Header from '../../components/header/header';
import colorsTheme from '../../utils/colorsTheme';
import { API_PREFIX } from '../../config/config';
import Text from '../../components/text/text';
import { moderateScale, scale, verticalScale } from '../../utils/scaling';
import GradientView from '../../components/gradientView';
import Swiper from 'react-native-swiper';
import Strings from '../../locales/strings';
import Font from '../../utils/fonts';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { homeDrawer } from '../../navigation/config/navigator';
import authContext from '../../context/authContext';
function HomeScreen({ navigation }: any) {
  const [categoryData, setData] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const { authData, setAuthData } = useContext(authContext);
  const authToken = authData
  console.log(authToken, "AuthContext")
  useEffect(() => {
    fetch(API_PREFIX)
      .then(response => response.json())
      .then(categoryData => setData(categoryData))
      .catch(error => console.error(error))
      .finally(() => setLoading(false));
    console.log(categoryData, 'categoryData');
  }, []);

  return (
    <View style={styles.scroll}>
      <Header navigation={navigation} />
      {categoryData.length != 0 ? (
        <View style={styles.container}>
          <Swiper
            autoplay
            dotColor={colorsTheme.colors.white}
            activeDotColor={colorsTheme.colors.primary}
            dotStyle={[styles.Dot, { borderColor: colorsTheme.colors.primary }]}
            activeDotStyle={styles.Dot}>
            <View style={styles.slide}>
              <Image
                resizeMode={'cover'}
                source={require('../../assets/images/slide.png')}
                style={styles.imageBg}
              />
              <View style={styles.containerTxtDiscount}>
                <Text
                  style={styles.txtDiscountPercent}
                  title={Strings.discountPercent}
                />
                <Text style={styles.txtDiscount} title={Strings.Discount} />
              </View>
            </View>
            <View style={styles.slide}>
              <Image
                resizeMode={'cover'}
                source={require('../../assets/images/slide.png')}
                style={styles.imageBg}
              />
              <View style={styles.containerTxtDiscount}>
                <Text
                  style={styles.txtDiscountPercent}
                  title={Strings.discountPercent}
                />
                <Text style={styles.txtDiscount} title={Strings.Discount} />
              </View>
            </View>
            <View style={styles.slide}>
              <Image
                resizeMode={'cover'}
                source={require('../../assets/images/slide.png')}
                style={styles.imageBg}
              />
              <View style={styles.containerTxtDiscount}>
                <Text
                  style={styles.txtDiscountPercent}
                  title={Strings.discountPercent}
                />
                <Text style={styles.txtDiscount} title={Strings.Discount} />
              </View>
            </View>
          </Swiper>
          <FlatList
            style={styles.flatList}
            data={categoryData}
            numColumns={2}
            // @ts-ignore
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }: any) => (
              <TouchableOpacity
                key={item.toString()}
                style={styles.cardContainer}
                onPress={() =>
                  navigation.navigate(homeDrawer.foodDetails, {
                    products: item.products,
                    cardImg: item.category_img,
                    cardName: item?.name,
                  })
                }>
                <GradientView Style={styles.gradientView}>
                  <Text title={item?.name} style={styles.txtNameCategory} />
                </GradientView>
                <Image source={{ uri: item.category_img }} style={styles.Image} />
              </TouchableOpacity>
            )}
          />
        </View>
      ) : (
        <ActivityIndicator
          style={{ marginTop: moderateScale(200) }}
          size="small"
          color={colorsTheme.colors.primary}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  scroll: {
    flex: 1,
    backgroundColor: colorsTheme.colors.white,
  },
  container: {
    alignSelf: 'center',
    alignItems: 'center',
  },
  slide: {
    alignSelf: 'center',
    alignItems: 'center',
    width: '100%',
  },
  imageBg: {
    width: '100%',
  },
  Dot: {
    borderWidth: 1,
    height: 15,
    width: 15,
    borderRadius: 50,
    top: moderateScale(12),
  },
  flatList: {
    marginTop: moderateScale(7),
    paddingHorizontal: moderateScale(25),
    alignSelf: 'center',
  },
  Image: {
    height: verticalScale(159),
    width: scale(160),
    borderRadius: moderateScale(5),
    position: 'relative',
  },
  txtNameCategory: {
    color: colorsTheme.colors.white,
    fontSize: moderateScale(20),
    alignItems: 'center',
    zIndex: 1,
    top: 110,
    paddingHorizontal: moderateScale(10),
  },
  txtDiscount: {
    color: colorsTheme.colors.white,
    fontSize: moderateScale(20),
    zIndex: 1,
    paddingHorizontal: moderateScale(20),
  },
  txtDiscountPercent: {
    color: colorsTheme.colors.white,
    fontSize: moderateScale(50),
    zIndex: 1,
    paddingHorizontal: moderateScale(20),
    fontFamily: Font.Family.semi_bold,
    fontWeight: 'bold',
  },
  cardContainer: {
    margin: moderateScale(3),
  },
  gradientView: { height: verticalScale(159), width: scale(160), zIndex: 1 },
  containerTxtDiscount: {
    position: 'absolute',
    left: 30,
    top: 50,
  },
});

export default HomeScreen;
function AuthContext(AuthContext: any): { authData: any; setAuthData: any; } {
  throw new Error('Function not implemented.');
}

