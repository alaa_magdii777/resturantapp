const colorsTheme = {
  dark: false,
  colors: {
    primary: '#D95500',
    white: 'white',
    transparent: 'transparent',
    black: '#000000',
    gray: '#E4E4E4',
    red: 'red',
    lightGray:'#EFEFEF'
  },
};

export default colorsTheme;
