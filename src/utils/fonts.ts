import {I18nManager} from "react-native"
 
 const fontTheme_ar = {
    Family: {
        regular:"Poppins-Regular",
        medium:"Poppins-Medium",
        semi_bold:"Poppins-SemiBold"
    },
  };
  
  const fontTheme_en = {
    Family: {
        regular:"Poppins-Regular",
        medium:"Poppins-Medium",
        semi_bold:"Poppins-SemiBold"
    },
  };
     
  const Font=I18nManager.isRTL?fontTheme_ar:fontTheme_en
  
  export default Font;


