import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {moderateScale} from '../utils/scaling';

const GradientView = ({children, Style = []}: any) => {
  return (
    <LinearGradient
      colors={['#00000000', '#000000AF']}
      start={{x: 0, y: 0}}
      end={{x: 0.3, y: 1}}
      style={[styles.header_container, Style]}>
      {children}
    </LinearGradient>
  );
};

export default GradientView;

const styles = StyleSheet.create({
  header_container: {
    borderRadius: moderateScale(5),
    position: 'absolute',
    left: 0,
    opacity: 1,
    resizeMode: 'cover',
    zIndex: 1,
  },
});
