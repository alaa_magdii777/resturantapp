import React from 'react';
import { FlatList, ScrollView, View, Image, StyleSheet } from 'react-native';
import DrawerRow from './drawerRow';
import { scale, verticalScale, moderateScale } from '../../utils/scaling';
import Font from '../../utils/fonts';
import colorsTheme from '../../utils/colorsTheme';
import Text from '../text/text';
import Strings from '../../locales/strings';
import { authStack, homeDrawer, rootSwitch } from '../../navigation/config/navigator';
import Icon from 'react-native-vector-icons/FontAwesome';

const DrawerContent = ({ navigation }: any) => {
  const DrawerData = [
    {
      id: 1,
      nameEn: Strings.home,
      nameNav: rootSwitch.drawer,
      Screen: homeDrawer.home,
      activeImg: <Icon name="home" color={colorsTheme.colors.primary} />,
    },
    {
      id: 2,
      nameEn: Strings.foodOptions,
      nameNav: rootSwitch.drawer,
      Screen: homeDrawer.foodOption,
      activeImg: <Icon name="file" color={colorsTheme.colors.primary} />,
    },
    {
      id: 3,
      nameEn: Strings.Logout,
      nameNav: authStack.login,
      // activeImg: <Icon name="file" color={colorsTheme.colors.primary} />,
    }
  ];
  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <View style={{ marginHorizontal: scale(15) }}>
          <Image
            style={styles.PlaceHolder}
            source={require('../../assets/icons/logo.png')}
          />
          <Text style={styles.txt} title={Strings.Ownername} />
        </View>
      </View>
      <View>
        <FlatList
          data={DrawerData}
          renderItem={({ item, index }: any) => (
            <DrawerRow item={item} index={index} navigation={navigation} />
          )}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logoContainer: {
    width: '100%',
    height: verticalScale(144),
    backgroundColor: colorsTheme.colors.primary,
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop: verticalScale(10),
  },
  PlaceHolder: {
    height: verticalScale(56),
    width: scale(56),
    resizeMode: 'contain',
  },
  txt: {
    marginTop: moderateScale(10),
    fontSize: moderateScale(24),
    fontFamily: Font.Family.regular,
  },
});
export default DrawerContent;
