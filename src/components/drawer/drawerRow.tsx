import AsyncStorage from '@react-native-community/async-storage';
import React, { useContext } from 'react';
import { TouchableOpacity, Text, StyleSheet, Image, View } from 'react-native';
import colorsTheme from '../../utils/colorsTheme';
import Font from '../../utils/fonts';
import { verticalScale, scale, moderateScale } from '../../utils/scaling';

const DrawerRow = ({ item, index, navigation }: any) => {
  const { activeImg, isSelected, nameEn, nameNav, Screen } = item;
  return (
    <React.Fragment>
      {index === 2 ?
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.activeRowContainer}
          onPress={() => {
            AsyncStorage.clear()
            navigation.navigate(nameNav, { screen: Screen });
          }}>
          <View>{activeImg}</View>
          <Text style={styles.activeText}>{nameEn}</Text>
        </TouchableOpacity>
        : <TouchableOpacity
          activeOpacity={0.8}
          style={styles.activeRowContainer}
          onPress={() => {
            navigation.navigate(nameNav, { screen: Screen });
          }}>
          <View>{activeImg}</View>
          <Text style={styles.activeText}>{nameEn}</Text>
        </TouchableOpacity>
      }
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  activeRowContainer: {
    width: scale(545),
    height: verticalScale(37),
    marginVertical: verticalScale(7),
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: scale(20),
  },
  RowContainer: {
    width: scale(545),
    height: verticalScale(37),
    marginVertical: verticalScale(7),
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: scale(20),
  },
  activeText: {
    fontSize: moderateScale(14),
    marginHorizontal: scale(7),
    color: colorsTheme.colors.primary,
    fontFamily: Font.Family.medium,
  },
});

export default DrawerRow;
