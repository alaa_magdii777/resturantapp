import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import colorsTheme from '../../utils/colorsTheme';
import {moderateScale, scale, verticalScale} from '../../utils/scaling';
import Text from '../text/text';

export default function Header({
  navigation,
  Style = [],
  headerScreen,
  title,
}: any) {
  return (
    <View style={[styles.container, Style]}>
      <React.Fragment>
        {!headerScreen ? (
          <TouchableOpacity onPress={() => navigation.openDrawer()}>
            <Image
              style={styles.ImgMenu}
              source={require('../../assets/icons/menu.png')}
            />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              style={styles.ImgBack}
              source={require('../../assets/icons/back.png')}
            />
          </TouchableOpacity>
        )}
      </React.Fragment>
      {/* {headerScreen ? <Text title={title} style={styles.title} /> : null} */}
      <>
        {!headerScreen ? (
          <View style={styles.row}>
            <Image
              style={[styles.Img, {marginHorizontal: moderateScale(5)}]}
              source={require('../../assets/icons/search.png')}
            />
            <Image
              style={styles.Img}
              source={require('../../assets/icons/shopping-cart.png')}
            />
          </View>
        ) : (
          <View style={styles.row}>
            <Text title={title} style={styles.title} />
            <Image
              style={[styles.Img, {marginHorizontal: moderateScale(10)}]}
              source={require('../../assets/icons/searchWhite.png')}
            />

            <Image
              style={styles.Img}
              source={require('../../assets/icons/shopping-cartWhite.png')}
            />
          </View>
        )}
      </>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: verticalScale(70),
    backgroundColor: colorsTheme.colors.white,
    padding: moderateScale(20),
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
  },
  Img: {
    height: verticalScale(21),
    width: scale(21),
    resizeMode: 'contain',
  },
  ImgMenu: {
    height: verticalScale(14),
    width: scale(28),
    resizeMode: 'contain',
  },
  ImgBack: {
    height: verticalScale(20),
    width: scale(20),
    resizeMode: 'contain',
  },
  title: {
    color: colorsTheme.colors.white,
    fontSize: moderateScale(22),
    marginRight: moderateScale(10),
  },
});
