import React from 'react';
import {View, Text as TextComponent, StyleSheet} from 'react-native';
import colorsTheme from '../../utils/colorsTheme';
import Font from '../../utils/fonts';
import {moderateScale} from '../../utils/scaling';

export default function Text({style = [], title}: any) {
  return (
    <View>
      <TextComponent style={[styles.mainStyle, style]}>{title}</TextComponent>
    </View>
  );
}

const styles = StyleSheet.create({
  mainStyle: {
    color: colorsTheme.colors.black,
    fontFamily: Font.Family.regular,
    fontSize: moderateScale(20),
  },
});
