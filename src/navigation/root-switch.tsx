import * as React from 'react';
import IntroStack from './intro-stack';
import HomeStack from './home-stack';
import SplashScreen from 'react-native-splash-screen';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {rootSwitch} from './config/navigator';
import authStack from './auth-stack';
const Stack = createStackNavigator();

const AppNavigator = (props: any) => {
  SplashScreen.hide();

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        {/* <Stack.Screen name={rootSwitch.intro} component={IntroStack} /> */}
        <Stack.Screen name={rootSwitch.auth} component={authStack} />
        <Stack.Screen name={rootSwitch.home} component={HomeStack} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
