export const introStack = {
  introS: 'introScreen',
};

export const authStack = {
  login: 'LoginScreen',
  signUp:"SignUpScreen"
};

export const homeStack = {
  drawer: 'homeDrawer',
};

export const homeDrawer = {
  home: 'HomeScreen',
  foodOption: 'foodOptionScreen',
  foodDetails: 'FoodDetails',
};

export const rootSwitch = {
  intro: 'IntroStack',
  auth: 'AuthStack',
  main: 'MainStack',
  home: 'HomeStack',
  drawer: 'homeDrawer',
};
