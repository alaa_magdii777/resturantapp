import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {introStack} from './config/navigator';
import introScreen from '../screens/intro/introScreen';

const Stack = createStackNavigator();

export default ({name}: any) => {
  return (
    //@ts-ignore
    <Stack.Navigator name={name} headerMode="none">
      <Stack.Screen name={introStack.introS} component={introScreen} />
    </Stack.Navigator>
  );
};
