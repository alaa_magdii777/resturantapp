import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {authStack} from './config/navigator';
import LoginScreen from '../screens/auth/login';
import SignUpScreen from '../screens/auth/signUp';

const Stack = createStackNavigator();

export default ({name}: any) => {
  return (
    //@ts-ignore
    <Stack.Navigator name={name} headerMode="none">
      <Stack.Screen name={authStack.login} component={LoginScreen} />
      <Stack.Screen name={authStack.signUp} component={SignUpScreen} />
    </Stack.Navigator>
  );
};
