import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {homeStack} from './config/navigator';
import homeDrawer from './home-drawer';
import HomeScreen from '../screens/home';

const Stack = createStackNavigator();

export default ({name}: any) => {
  return (
    //@ts-ignore
    <Stack.Navigator name={name} headerMode="none">
      <Stack.Screen name={homeStack.drawer} component={homeDrawer} />
    </Stack.Navigator>
  );
};
