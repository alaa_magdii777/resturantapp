import React from 'react';
import {Dimensions} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {homeDrawer} from './config/navigator';
import HomeScreen from '../screens/home';
import DrawerContent from '../components/drawer/drawerContent';
import foodOptionScreen from '../screens/food/foodOptionScreen';
import FoodDetails from '../screens/home/foodDetails';

const Drawer = createDrawerNavigator();
const {width} = Dimensions.get('window');

const WIDTH_DRAWER = width * 0.78;

const drawerStyle = {
  width: WIDTH_DRAWER,
};
export default ({navigation}: any) => {
  return (
    <Drawer.Navigator
      drawerContent={props => (
        <DrawerContent {...props} navigation={navigation} />
      )}
      initialRouteName={homeDrawer.home}
      screenOptions={{headerShown: false}}
      drawerStyle={drawerStyle}>
      <Drawer.Screen name={homeDrawer.home} component={HomeScreen} />
      <Drawer.Screen
        name={homeDrawer.foodOption}
        component={foodOptionScreen}
      />
      <Drawer.Screen name={homeDrawer.foodDetails} component={FoodDetails} />
    </Drawer.Navigator>
  );
};
