import { WebApi } from "../web-api.service";

export class userLoginApiService {
    private webApi = new WebApi();
    private controller = "loginUser";
    
    async loginUser(body: any) {
        return await this.webApi.Post(`${this.controller}`,body);
    }
}