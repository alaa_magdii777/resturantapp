import { WebApi } from "../web-api.service";

export class userRegisterApiService {
    private webApi = new WebApi();
    private controller = "registerUser";

    async registerUser(body: any) {
        return await this.webApi.Post(`${this.controller}`,body);
    }
}